#include "debug.h"

FILE* debug_file_stream;

void enable_debug() {
    debug_file_stream = fopen(DEBUG_STREAM, "w");
}

void debug(const char* message, ...) {
    va_list v;
    va_start(v, message);
    vfprintf(debug_file_stream, message, v);
    fflush(debug_file_stream);
    va_end(v);
}

void close_debug() {
    fclose(debug_file_stream);
}