#include "core.h"
#include "debug.h"
#include "http.h"
#include "kernel.h"
#include "error.h"

void daemonize(const char* ch_dir_to) {
    pid_t pid, sid;  
    int fd;   
    /* already a daemon */  
    if ( getppid() == 1 ) {
        return;
    }
  
    /* Fork off the parent process */  
    pid = fork();  
    if (pid < 0)    
    {
        log_err("fork failure");
        exit(EXIT_FAILURE);  
    }     
  
    if (pid > 0)    
    {     
        fprintf(stdout, "Forked successfully, parent exiting\n");
        /*Killing the Parent Process*/
        tear_down();
    }     
  
    /* At this point we are executing as the child process */
    FILE* f = fopen(PID_PATH, "w");
    fprintf(f, "%d", getpid());
    fclose(f);
  
    /* Create a new SID for the child process */  
    sid = setsid();  
    if (sid < 0)    
    {
        log_err("setsid failure");  
        exit(EXIT_FAILURE);  
    }  
  
    /* Change the current working directory. */  
    if (NULL != ch_dir_to && (chdir(ch_dir_to)) < 0)  
    {  
        log_err("chidir failure");
        exit(EXIT_FAILURE);  
    }  
  
    fd = open(DEV_NULL ,O_RDWR, 0);  
  
    if (fd != -1)  
    {  
        dup2 (fd, STDIN_FILENO);  
        dup2 (fd, STDOUT_FILENO);  
        dup2 (fd, STDERR_FILENO);  
  
        if (fd > 2)  
        {  
            close (fd);  
        }  
    }  
  
    /*resetting file creation mask */  
    umask(027);
}

int bind_new_socket() {
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        log_err("socket call failure");
        exit(EXIT_FAILURE);
    }
    struct sockaddr_un s;
    memset(&s, 0, sizeof(s));
    s.sun_family = AF_UNIX;
    strcpy(s.sun_path, SERVER_PATH);
    fprintf(stdout, "Binding unix socket to: %s\n", s.sun_path);
    if (bind(fd, (struct sockaddr*) &s, sizeof(s)) == -1) {
        fs_error("bind call failure");
    }

    return fd;
}

int main(int argc, char** argv) {
    init();
    int fd, cl, rc;
    char buf[MAX_REQUEST_SIZE];
    fd = bind_new_socket();
    Logger* logger = create_default_logger();

    if (listen(fd, REQUEST_CONCURRENCY) == -1) {
        fs_error("listen error");
    }
    daemonize(NULL);

    while (1) {
        if ( (cl = accept(fd, NULL, NULL)) == -1) {
            log_err(logger, "accept error");
            continue;
        }
        HttpRequest* request;

        while ( (rc=read(cl, buf ,sizeof(buf))) > 0) {
            log_debug(logger, "%d%s", rc, buf);
            request = parse_request(buf);
            
        }


//        HttpResponse* response = process_request(request);
//        char output_buffer[400];
//
//        sprintf(output_buffer, "HTTP/%i.%i %i OK\r\ncontent-type: text/html; charset=UTF-8\r\n\r\n%s\r\n",
//            response->version.major_version,
//            response->version.minor_version,
//            response->status_code,
//            response->message
//        );
//
//        debug("Trying to write output!\n");
//        debug(output_buffer, "HTTP/%i.%i %i OK\r\ncontent-type: text/html; charset=UTF-8\r\n\r\n%s\r\n",
//            response->version.major_version,
//            response->version.minor_version,
//            response->status_code,
//            response->message);
//
//        int sc = send(fd, output_buffer, 300, 0);
//        debug("Sent %i characters\n", sc);

        if (rc == -1) {
            fs_error("read");
        }
        else if (rc == 0) {
//            send(fd, "EOF\n", 4, 0);
            close(cl);
        }
    }


    if (unlink(SERVER_PATH) == -1) {
        log_err(logger, "socket not removed");
    }
    
    tear_down();
}
