#pragma once
#include "core.h"

#define DEBUG_STREAM "/tmp/debug"

void enable_debug();

void debug(const char*, ...);

void close_debug();