#include "http.h"
#include "debug.h"

HttpMethod determine_method(char* buff, int* index) {
    switch (buff[(*index)++]) {
        case 'G':
            (*index) += 2;
            return GET;
        case 'H':
            (*index) += 3;
            return HEAD;
        case 'P':
            switch (buff[(*index++)]) {
                case 'O':
                    return POST;
                case 'U':
                    (*index)++;
                    return PUT;
                default: 
                    debug("Malformed http request, first character is P and second character is %c\n", buff[(*index++)]);
                    (*index)++;
                    return GET;
            }
        case 'D':
            (*index) += 5;
            return DELETE;
        case 'C':
            (*index) += 6;
            return CONNECT;
        case 'O':
            (*index) += 6;
            return OPTIONS;
        case 'T':
            (*index) += 4;
            return TRACE;
        default:
            debug("Malformed http request, first character is %c\n", buff[(*index)++]);
            (*index) += 2;
            return GET;
    }
}

HttpUriType determine_uri_type(char* buff, int* index) {
    switch (buff[(*index)++]) {
        case '*':
            return ASTERIKS;
        case '/':
            return ABS_PATH;
        default:
            // Too many possible formats to validate in this place.
            return ABSOLUTE_URI;
    }
}

void validate_http_word(char* buff, int* index) {
    if (buff[(*index)++] != 'H') debug("Malformed http request, expected: 'H', got '%c'\n", buff[(*index) - 1]);
    if (buff[(*index)++] != 'T') debug("Malformed http request, expected: 'T', got '%c'\n", buff[(*index) - 1]);
    if (buff[(*index)++] != 'T') debug("Malformed http request, expected: 'T', got '%c'\n", buff[(*index) - 1]);
    if (buff[(*index)++] != 'P') debug("Malformed http request, expected: 'P', got '%c'\n", buff[(*index) - 1]);
}

HttpVersion determine_version(char* buff, int* index) {
    validate_http_word(buff, index);
    if (buff[(*index)++] != '/') debug("Malformed http request, expected: '?', got '%c'\n", buff[(*index) - 1]);
    HttpVersion version;
    char major = buff[(*index)++];
    debug("Got major version: %c\n", major);
    version.major_version = major - '0';
    if (buff[(*index)++] != '.') debug("Malformed http request, version is bad\n");
    char minor = buff[(*index)++];
    debug("Got minor version: %c\n", minor);
    version.minor_version = minor - '0';

    return version;
}

HttpSchema determine_schema(char* buff, int* index) {
    validate_http_word(buff, index);

    switch (buff[(*index)++]) {
        case ':':
            return HTTP;
        case 'S':
            if (buff[(*index)++] != ':') debug("Malformed http request, schema is bad\n"); 
            return HTTPS;
        default:
            debug ("Malformed http request, 5th schema character: %c\n", buff[(*index)-1]);
            return UNSUPPORTED;
    }
}

void set_request_uri(HttpRequestUri* request_uri, char* buff, int* index) {
    if (request_uri->type == ASTERIKS) {
        request_uri->path = NULL;

        return;
    }

    if (request_uri->type == ABS_PATH) {
        char* path = (char*)malloc(MAX_PATH_LENGTH*sizeof(char));
        for (int i = 0; i < MAX_PATH_LENGTH; i++) {
            if (buff[(*index)] == ' ') {
                request_uri->path = path;
            }
            path[i] = buff[(*index)++];
        }
        debug("Malformed http request, abs path is longer than 4096 characters\n");
        request_uri->path = path;

        return;
    }

    HttpSchema schema = determine_schema(buff, index);
    debug("Found schema: %i\n", schema);
    debug("TODO: Implement url parsing, format / [subdomain.] domain . tld /");
    tear_down();
}


HttpRequest* parse_request(char* buff) {
    int index = 0;
    HttpRequest* request = malloc(sizeof(HttpRequest));
    request->method = determine_method(buff, &index);

    if (buff[index++] != ' ') debug("Malformed http request, expected character is not an empty space, but: %c\n", buff[index-1]);

    HttpRequestUri request_uri;
    request_uri.type = determine_uri_type(buff, &index);
    request->uri = request_uri;
    if (buff[index++] != ' ') debug("Malformed http request, expected character is not an empty space, but: %c\n", buff[index-1]); 
    request->version = determine_version(buff, &index);
    
    return request;
}