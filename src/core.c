#include "core.h"
#include "debug.h"

void sig_term_handler(int signum, siginfo_t *info, void *ptr)
{
    debug("Sigterm received, exiting.");
    if (unlink(PID_PATH) == -1) perror("failed to delete pid file");
    if (unlink(SERVER_PATH) == -1) perror("failed to delete socket");
    write(STDERR_FILENO, SIGTERM_MSG, sizeof(SIGTERM_MSG));

    tear_down();
}

void catch_sigterm()
{
    static struct sigaction _sigact;

    memset(&_sigact, 0, sizeof(_sigact));
    _sigact.sa_sigaction = sig_term_handler;
    _sigact.sa_flags = SA_SIGINFO;

    sigaction(SIGTERM, &_sigact, NULL);
}


void init() {
    enable_debug();
    catch_sigterm();
}

void tear_down() {
    close_debug();
    exit(EXIT_SUCCESS);
}