#include "message.h"

Message* create_message(size_t size, const char* fmt, ...) {
    va_list v;
    va_start(v, fmt);
    char* buffer = malloc(sizeof(size));
    vsprintf(buffer, fmt, v);
    va_end(v);
    Message* message = malloc(sizeof(Message));
    message->content = buffer;
    message->size = size;

    return message;
}

void delete_message(Message* message) {
    free(message->content);
    free(message);
}