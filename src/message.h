#pragma once
#include "core.h"

typedef struct Message {
    char* content;
    size_t size;
} Message;

Message* create_message(size_t, const char*, ...);

void delete_message(Message*);
