#include "error.h"

void fs_error(const char* msg) {
    log_err(msg);
    if (unlink(SERVER_PATH) == -1) {
        log_err("failed to remove socket");
    }
    exit(EXIT_FAILURE);
}
