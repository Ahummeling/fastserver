#include "kernel.h"
#include "debug.h"

HttpResponse* process_request(HttpRequest* request) {
    debug("Method: %i, path: %s, version: HTTP/%i.%i\n", request->method, request->uri.path, request->version.major_version, request->version.minor_version);
    HttpResponse* response = malloc(sizeof(HttpResponse));
    response->version = request->version;
    response->status_code = OK;
    response->message = "Hello, world!\n";
    free(request);

    return response;
}