#pragma once
#include "sys/socket.h"
#include "sys/un.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "signal.h"
#include "stdarg.h"
#include "logger.h"

#define SIGTERM_MSG "SIGTERM received. Exiting gracefully.\n"
#define DEV_NULL "/dev/null"
#define SERVER_PATH "/run/fastserver/fastserver.sock"
#define PID_PATH "/run/fastserver/server.pid"
#define REQUEST_CONCURRENCY 1
// #define MAX_REQUEST_SIZE 4294967296 // LOL this causes a segfault.
#define MAX_REQUEST_SIZE 65536
#define MAX_PATH_LENGTH 4096

void sig_term_handler(int signum, siginfo_t *info, void *ptr);

void catch_sigterm();

void init();

void tear_down();