#pragma once
#include "core.h"

typedef enum VERB {
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE
} HttpMethod;

typedef enum STATUS_CODE {
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202
    /* TODO: complete list */
} HttpStatusCode;

typedef enum URI_TYPE {
    ASTERIKS,
    ABSOLUTE_URI,
    ABS_PATH
} HttpUriType;

typedef enum URI_SCHEMA {
    HTTP,
    HTTPS,
    UNSUPPORTED
} HttpSchema;

typedef struct HttpRequestUri {
    HttpUriType type;
    char* path;
} HttpRequestUri;

typedef struct HttpVersion {
    int major_version;
    int minor_version;
} HttpVersion;

typedef struct HttpRequest {
    HttpMethod method;
    HttpRequestUri uri;
    HttpVersion version;
} HttpRequest;

typedef struct HttpResponse {
    HttpStatusCode status_code;
    char* message;
    HttpVersion version;
} HttpResponse;

HttpRequest* parse_request(char*);