.PHONY: cleanbuild
cleanbuild:
	$(MAKE) clean
	$(MAKE) fastserver

fastserver:
	gcc -o fastserver src/*.c -Wall -Wpedantic -llogging

.PHONY: clean
clean:
	rm fastserver || true
