# Fast Server
C nginx upstream built from scratch to see how fast it can get. (WIP)

## Explanation
Program binds to a unix socket, forks child process, detaches child (so program daemonizes), and then listens to output. Output is currently send to a non-existing tty's stdout.
Sending the kill signal to the program causes it to delete its pidfile and socket file before closing.
Trying to run the program twice will result in silently skipping daemonization, thus allowing only a single instance of the program to run.

## Requirements:
write access to a directory at path `/run/fastserver/`. (You must create this directory yourself)

## Usage
Compile with: 
```
make
```

Run with:
```
./fastserver
```

Stop with:
```
./kill.sh
```

Listen with:
```
socat - UNIX-CONNECT:/run/fastserver/fastserver.sock
```


## References
 - https://medium.com/from-the-scratch/http-server-what-do-you-need-to-know-to-build-a-simple-http-server-from-scratch-d1ef8945e4fa
 - https://unix.stackexchange.com/questions/26715/how-can-i-communicate-with-a-unix-domain-socket-via-the-shell-on-debian-squeeze
 - https://github.com/troydhanson/network/blob/master/unixdomain/01.basic/srv.c
 - http://mihids.blogspot.com/2015/02/detaching-process-from-terminal-exec.html
 - https://pymotw.com/2/socket/uds.html
 - https://tools.ietf.org/html/rfc2616#section-7.2
 - https://docs.python.org/3/library/socket.html