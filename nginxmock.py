import socket
import sys
import os

# Create a UDS socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = '/run/fastserver/fastserver.sock'
print(f'connecting to {server_address}') 
try:
    sock.connect(server_address)
except socket.error as error:
    print(error)
    sys.exit(1)


try:
    print('sending message')

    with open('request.http', 'rb') as request:
        sock.sendfile(request)

        amount_received = 0
        amount_expected = 500
        
        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            print(f'received {data}')
except BaseException as e:
    print(e)

sock.close()