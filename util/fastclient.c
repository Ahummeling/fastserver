#include "sys/socket.h"
#include "sys/un.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#define SERVER_PATH "/run/fastserver/fastserver.sock"

int connect_to_socket() {
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        perror("socket call failure");
        exit(EXIT_FAILURE);
    }
    struct sockaddr_un s;
    memset(&s, 0, sizeof(s));
    s.sun_family = AF_UNIX;
    strcpy(s.sun_path, SERVER_PATH);
    fprintf(stdout, "Binding unix socket to: %s\n", s.sun_path);
    if (connect(fd, (struct sockaddr*) &s, sizeof(s)) == -1) {
        perror("bind call failure");
        if (unlink(SERVER_PATH) == -1) {
            perror("socket not removed");
        }
        exit(EXIT_FAILURE);

    }

    return fd;
}

int main(int argc, char** argv) {
    int fd = connect_to_socket;
}